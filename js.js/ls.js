const name = document.querySelector('#name');
const age = document.querySelector('#age');//по ай ди
const addBtn = document.querySelector('.add');//по классу
const clearBtn = document.querySelector('.clear');
const deleteBtn = document.querySelector('.delete');


addBtn.addEventListener('click', addHandler);
clearBtn.addEventListener('click',clearUserList);
deleteBtn.addEventListener('click', deleteLastItem);


console.log(getUserList());

function getUserObj() {
    const userName = name.value;
    const userAge = age.value;
    const userObj = {
        name: userName,
        age: userAge
    };
    return userObj;
} 



function clearFields() {
    name.value = '';
    age.value = '';
}



function addHandler() {
    const userObj = getUserObj();
    clearFields();
   const users = getUserList();
   users.push(userObj);
   saveUserList(users);

 

}

function getUserList() {
    const lsData = localStorage.getItem('users');
    if(lsData) {
        const parsData = JSON.parse(lsData);//преобразовываем в json
      
        return parsData;
    }
    return [];
}




function saveUserList(users) {
const stringiFyList = JSON.stringify(users);
localStorage.setItem('users', stringiFyList);
}



function clearUserList() {
localStorage.removeItem('users');

}



function deleteLastItem() {
    const users = getUserList();
    if(users.length > 0) {
        users.pop();
        saveUserList(users);
    }
}


//HTTP: WWW.TEST.RU./API/USRES?FROM =10&TO=30&RESULT=5  ПОЛУЧИТЬ СПИСОК ПОЛЬЗОВАТЕЛЬ
 
//запросы
//GET, POST , PATCH, DELETE -УДАЛИТЬ
//GET ПОЛУЧИТЬ
//POST -СОХРАНИТЬ
//PATCH -ИЗМЕНИТЬ