//HTTP: WWW.TEST.RU./API/USRES?FROM =10&TO=30&RESULT=5  ПОЛУЧИТЬ СПИСОК ПОЛЬЗОВАТЕЛЬ

//запросы
//GET, POST , PATCH, DELETE -УДАЛИТЬ
//GET ПОЛУЧИТЬ
//POST -СОХРАНИТЬ
//PATCH -ИЗМЕНИТЬ
//PUT


//promise


const container = document.querySelector('.container');
let myUserList = [];
const xhr = new XMLHttpRequest();//для работы с бекэндом

xhr.open('GET', 'https://randomuser.me/api?results=10');

xhr.onreadystatechange = () => {//вся информация о запросе на сервер

    if (xhr.readyState === 4) {
        if (xhr.status === 200) {
            const data = JSON.parse(xhr.responseText);
            const userList = data.results;



            myUserList = convertUserData(userList);
            console.log(myUserList);
            drawUsers();


        }
    }
}                           //альтрнатива слушателя

xhr.send();



function drawUsers() {
    container.innerText = '';
    myUserList.forEach(item => {
        const userTag = getUserCard(item);
        container.appendChild(userTag);
    });
}


function getUserCard(user) {
    const card = document.createElement('div');
    card.classList.add('card');

    const avatar = document.createElement('img');
    avatar.classList.add('avatar');
    avatar.src = user.avatar;


    const info = document.createElement('div');
    info.classList.add('info');


    const userName = document.createElement('span');
    userName.classList.add('user-name');
    userName.innerText = user.name;


    const deleteBtn = document.createElement('div');
    deleteBtn.classList.add('delete');
    deleteBtn.innerText = 'x';
    const deleteHandler = () => deleteItem(user.id);


    deleteBtn.addEventListener('click', deleteItem);




    info.appendChild(userName);

    card.appendChild(deleteBtn);

    card.appendChild(avatar);
    card.appendChild(info);
    return card;
}



function deleteItem(id) {
const user = myUserList.find(item => item.id === id);
const index = myUserList.indexOf(user);
myUserList.splice(index, 1);
drawUsers();
}

function convertUserData(users) {
    return users.map(item => {
        return {
            id: item.login.uuid,
            name: item.name.first + ' ' + item.name.last,
            avatar: item.picture.large
        }
    });
}